<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Details;
use App\Models\Detailsimage;
use Illuminate\Support\Facades\DB;

class DetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        // $data = Details::groupBy('uniq_id')->get();
        $data = Details::select("*", DB::raw("count(*) as totalrow"))
        ->groupBy('uniq_id')
        ->get();
        
        // dd($data);

        return view('details-list', ['data' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        
        return view('details-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // print_r($_FILES); 
        $number = mt_rand(10000, 99999);
        $section = $_POST['ssection'];

        if(count($section) > 0) {
            foreach($section as $key=>$val) {
                $name = $_POST["name".$val];
                $link = $_POST["link".$val];
                $image = $_FILES["image".$val];
                $description = $_POST["description".$val];
                
                $details = new Details; 
                $details->name=$name;
                $details->description=$description;
                $details->section=$key + 1;
                $details->uniq_id=$number;
                $details->save(); 

                $d_id = $details->id;
                
                if(count($image) > 0)
                {
                    foreach($link as $k=>$lnk) {
                        $img = $image['name'][$k];
                        $imgt = $image['tmp_name'][$k];
                        $lik = $lnk;
                        
                        $images = new Detailsimage;
                        $images->link = $lik;
                        $images->image = $img;
                        $images->image = $img;
                        $images->details_id = $d_id;
                        $images->save();
                        copy($imgt,public_path('images')."/".$img);
                     
                   }
               }


            }
        }
     
        return  redirect('/');
       
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Id =  base64_decode($id);
       $updatedata = Details::with('image')->where('uniq_id',$Id)->get();
    //    print_r($updatedata);die;
       //    $getimages = Detailsimage::where('details_id',$id)->all()->get();

       return view('details-edit', ['updatedata' => $updatedata,'']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $detailsdata = Details::all()->where('uniq_id',$request->uniq_id)->pluck('id');
   
        $ss_id = $_POST['ssection'];
        print_r($detailsdata);

        foreach($detailsdata as $key=>$dd)
        {
            $link=array();
            $img=array();
            //print_r($_POST['oldssection']);
           // print_r($dd);
      
           if(in_array($dd,$_POST['oldssection']))
           {

                $name = $_POST["name".$dd];                   
                $description = $_POST["description".$dd];

               // echo $dd;

                $details = Details::find($dd);
                $details->name=$name;
                $details->description=$description;
                $details->save(); 


                $detailsimgdata = Detailsimage::all()->where('details_id',$dd)->pluck('section');

               

                $deleteimg =  DB::table('details_images')->where('details_id', $dd)->delete();
                // die;
                $oldimges = array();

                $link = $_POST["link".$dd];            
                
                $image = $_FILES["image".$dd];
               if(isset($_POST["oldimage".$dd])) {
                 $oldimage = $_POST["oldimage".$dd];
               } else {
                $oldimage = array();
               }
              // print_r($link);
                    if(count($link) > 0)
                    {
                        foreach($link as $lk=>$lnk) {
                          

                            if($image['name'][$lk] !="" ) {
                        
                                $img = time()."_".$image['name'][$lk];
                                $imgt = $image['tmp_name'][$lk];
                                copy($imgt,public_path('images')."/".$img); 
                                
                            } else {
                               if(isset($oldimage[$lk])) {
                                    $img = $oldimage[$lk];
                               } else {
                                   $img = "";
                               }
                                
                            }
                           
                            $oldimges[] = $img;
                         
                           
                            if($lnk!="" || $img!="" ) {
                                $images = new Detailsimage;
                                $images->link = $lnk;
                                $images->image = $img;                          
                                $images->details_id = $dd;
                                $images->save();
                            }
                            
                            
                        
                    }
                }

                $img = Detailsimage::where('details_id',$dd)->get();
                
                foreach($img as $i )
                {   
                    if(file_exists(public_path('images')."/".$i->image) && $i->image != "" && !in_array($i->image, $oldimges))
                    {

                        unlink(public_path('images')."/".$i->image);
                    }
                }
                // die;
           }
           else
           {

                $img = Detailsimage::where('details_id',$dd)->get();
                
                foreach($img as $i )
                {
                    unlink(public_path('images')."/".$i->image);
                }

                $deleteimg =  DB::table('details_images')->where('details_id', $dd)->delete();
                $deletedetail =  DB::table('details')->where('id', $dd)->delete();
            
           }

        }

            $section = $_POST['ssection'];

            if(count($section) > 0) {
                foreach($section as $key=>$val) {
                    $name = $_POST["name".$val];
                    $link = $_POST["link".$val];
                    $image = $_FILES["image".$val];
                    $description = $_POST["description".$val];
                    if($name!="") {
                        $details = new Details; 
                        $details->name=$name;
                        $details->description=$description;
                        $details->section=$key + 1;
                        $details->uniq_id=$request->uniq_id;
                        $details->save(); 
    
                        $d_id = $details->id;

                        if(count($image) > 0)
                        {
                            foreach($link as $k=>$ilnk) {
                                $img =  time()."_".$image['name'][$k];
                                $imgt = $image['tmp_name'][$k];
                              
                                
                                $images = new Detailsimage;
                                $images->link = $ilnk;
                                $images->image = $img;
                                
                                $images->details_id = $d_id;
                                $images->save();
                                copy($imgt,public_path('images')."/".$img);
                            
                        }
                        
                    }
                   
                    
                    
                }


                }
            }

            
           
            

        return  redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destory($id)
    {
        $Id =  base64_decode($id);

        // echo $Id;die;
        
        $data = Details::where('uniq_id',$Id)->pluck('id')->all();

        foreach($data as $dd)
        {
            $img = Detailsimage::where('details_id',$dd)->get();
                
            foreach($img as $i )
            { 
                if(file_exists(public_path('images')."/".$i->image) && $i->image != "" )
                {

                    unlink(public_path('images')."/".$i->image);
                }
            }

            $deleteimg =  DB::table('details_images')->where('details_id', $dd)->delete();
            $deletedetail =  DB::table('details')->where('id', $dd)->delete();

        }

        return redirect('/');
    }
}
