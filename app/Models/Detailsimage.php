<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Detailsimage;


class Detailsimage extends Model
{
    use HasFactory;

    
    protected $fillable = [
        'link',
        'image'
      

    ];

    protected $table = "details_images";


    // public function image()
    // {
    //     return $this->hasMany(Detailsimage::class, 'details_id');
    // }
}
