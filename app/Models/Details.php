<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Detailsimage;

class Details extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'section',
        'description',

    ];

    protected $table = "details";

    public function image()
    {
        return $this->hasMany(Detailsimage::class);
    }
}
