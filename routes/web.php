<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DetailsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
    // return view('welcome');

    Route::get('/', [DetailsController::class, 'index']);
    Route::get('details-add', [DetailsController::class, 'add']);
    Route::post('details-store', [DetailsController::class, 'store']);
    Route::get('details-edit/{id}', [DetailsController::class, 'edit']);
    Route::post('details-update', [DetailsController::class, 'update']);
    Route::get('details-delete/{id}', [DetailsController::class, 'destory']);
// });
