<html>

<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</head>
<style>
    .hidden {
        display: none;
    }

</style>

<body>
    <h1>
        <center>Add Details</center>
    </h1>
    <section>
        <div class="container">
            {{-- <div class="container_clone_mainform "> --}}
            <div class="row  ">
                <form id="formValidate" method="POST"  action="{{ asset('details-store') }}" enctype="multipart/form-data" >
                    @csrf
                    <div class="container_clone_mainform">
                        <input type="hidden" id="sectionfld"  name="section" class="form-control form-control-sm sectionval"
                        id="colFormLabelSm" value="1">
                        <div class="subform">
                            <div class="form-group row">
                                <label for="colFormLabelSm"
                                    class="col-sm-2 col-form-label col-form-label-sm">Name</label>
                                <div class="col-sm-10">
                                    
                                    <input type="name" class="form-control form-control-sm fieldname" name="name1" id="colFormLabelSm"
                                        placeholder="name">
                                   
                                </div>
                            </div>
                            <div class="documents_main_container">
                                <div class="subsection">
                                    <div class="form-group row ddddd">
                                        <label for="colFormLabelSm"
                                            class="col-sm-2 col-form-label col-form-label-sm">Link</label>
                                        <div class="col-sm-10">
                                            <input type="link" class="form-control form-control-sm fieldlink" name="link1[]" id="colFormLabelSm"
                                                placeholder="link">

                                        </div>
                                    </div>
                                    <div class="form-group row  sdsdsdsd">
                                        <label for="colFormLabelSm"
                                            class="col-sm-2 col-form-label col-form-label-sm">Image</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control-file fieldimg" name="image1[]" id="exampleFormControlFile1">
                                            <div id="btnforadd">
                                                <button type="button"
                                                    class="btn btn-success add_more_namelink pull-right">Add
                                                    More</button>

                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="colFormLabelSm"
                                    class="col-sm-2 col-form-label col-form-label-sm">Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control fielddesc" id="exampleFormControlTextarea1" name="description1" rows="3"></textarea>
                                </div>

                                <div id="btnforaddform">

                                    <button type="button" class="btn btn-success add_more_section pull-right">Add
                                        More</button>
                                </div>
                                <input type="hidden"  name="ssection[]" class="form-control form-control-sm ssectionval"
                                  id="colFormLabelSm" value="1">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
            </div>
            </form>
        </div>
    </section>
    {{-- Clone name&link --}}
    <div class="container_clone hidden">
        <div class="subsection">
            <div class="form-group row">
                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Link</label>
                <div class="col-sm-10">
                    <input type="link" class="form-control form-control-sm fieldlink" id="colFormLabelSm" name="link[]" placeholder="link">

                </div>
            </div>
            <div class="form-group row">
                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Image</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control-file fieldimg" id="exampleFormControlFile1" name="image[]">
                    <div id="btnforadd">
                        <button type="button" class="btn btn-success add_more_namelink pull-right">Add
                            More</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    {{-- Clone name&link --}}
    {{-- Clone Full Form --}}
    <div class="container_clone_form hidden">
        <div class="subform">

            <div class="form-group row">
                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Name</label>
                <div class="col-sm-10">
                    <input type="name" class="form-control form-control-sm fieldname" id="colFormLabelSm" name="name" placeholder="name">
                   
                </div>
            </div>
            <div class="documents_main_container">
                <div class="subsection">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Link</label>
                        <div class="col-sm-10">
                            <input type="link" class="form-control form-control-sm fieldlink" name="link[]" id="colFormLabelSm"
                                placeholder="link">

                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Image</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file fieldimg" name="image[]" id="exampleFormControlFile1">
                            <div id="btnforadd">
                                <button type="button" class="btn btn-success add_more_namelink pull-right">Add
                                    More</button>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Description</label>
                <div class="col-sm-10">
                    <textarea class="form-control fielddesc" id="exampleFormControlTextarea1" name="description" rows="3"></textarea>
                </div>
                <div id="btnforaddform">
                    <button type="button" class="btn btn-success add_more_section pull-right">Add
                        More</button>
                </div>
                <input type="hidden"  name="ssection[]" id="ssectionid" class="form-control form-control-sm ssectionval"
                id="colFormLabelSm" value="">
            </div>

        </div>
    </div>
    {{-- Clone Full Form --}}
    <script type="text/javascript">
        $(document).ready(function() {


            $(document).off('click', '.add_more_namelink').on('click', '.add_more_namelink', function() {
               var sectionid = $("#sectionfld").val();
               var fieldlink = $(this).parent().parent().parent().parent().find('.fieldlink').attr('name');
               var fieldimg = $(this).parent().parent().parent().parent().find('.fieldimg').attr('name');

               $('.container_clone').find('.fieldlink').attr('name',fieldlink);
               $('.container_clone').find('.fieldimg').attr('name',fieldimg);
               $(this).parent().parent().parent().parent().parent().parent().find('.documents_main_container').append($('.container_clone').html());

                $(this).closest('#btnforadd').html('<button type="button" class="btn btn-danger delete_documents"><i class="fa fa-times">Remove</i></button>'
                );
             

            });

            $(document).off('click', '.delete_documents').on('click', '.delete_documents', function() {

                $(this).closest('.subsection').remove();
            });

            $(document).off('click', '.add_more_section').on('click', '.add_more_section', function() {

                var sectionid = parseFloat($("#sectionfld").val())+1;

            //    console.log(sectionid);
               $("#sectionfld").val(sectionid);

              
               var fieldnm = "name"+sectionid;
               var fieldlink = "link"+sectionid+"[]";
               var fieldimg = "image"+sectionid+"[]";
               var fielddesc = "description"+sectionid;
              
              

                $(this).closest('#btnforaddform').html(
                    '<button type="button" class="btn btn-danger delete_form"><i class="fa fa-times">Remove</i></button>'
                );
                var sddectionid =  $('.ssectionval:last').attr('id',"ssectionid"+sectionid);
             
                $('.container_clone_form').find('.fieldlink').attr('name',fieldlink);
                $('.container_clone_form').find('.fieldimg').attr('name',fieldimg);
                $('.container_clone_form').find('.fieldname').attr('name',fieldnm);
                $('.container_clone_form').find('.fielddesc').attr('name',fielddesc);
                $('.container_clone_form').find('.ssectionval').attr('id',"sss"+sectionid);
                var ssid =  $('.container_clone_form').find('.ssectionval').attr('id');
                
                document.getElementById(ssid).value = sectionid;

                $(".container_clone_mainform").append($('.container_clone_form').html());
               
               
                
               

            
                // $(".documents_main_container").find('select.tmpselect').select2();
                // $(".documents_main_container").find('.tmpselect').removeClass('tmpselect').addClass('select');
            });

            $(document).off('click', '.delete_form').on('click', '.delete_form', function() {
                //var sectionid = parseFloat($("#sectionfld").val())-1;
              //  $("#sectionfld").val(sectionid)
                $(this).closest('.subform').remove();
            });

        });

    </script>
</body>

</html>
