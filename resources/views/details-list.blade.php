
<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <body>
        <h1 style="text-align: center" >Details List</h1>
        <a href="{{ asset('details-add') }}"> Add</a>
        
        <div class="container">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                    @if(count($data) > 0)
                        @foreach ($data as $details)
                                                <tr>
                                <th scope="row">{{ $details->id }}</th>
                                <td>{{ $details->name }}</td>
                                <td>{{ $details->description }}</td>
                                <td>
                                
                                    <a href="{{ asset('details-edit/' . base64_encode($details['uniq_id'])) }}"><i
                                    class="material-icons">Edit</i></a> || 
                                    <a href="{{ asset('details-delete/' . base64_encode($details['uniq_id'])) }}"><i
                                    class="material-icons">Delete</i></a>
                                </td>
                            
                            </tr>
                        @endforeach
                    @else
                    <tr>
                      
                        <td colspan="4" align="center"><h1>No Record Found</h1> </td>
                    
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </body>
</html>